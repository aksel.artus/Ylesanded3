package ee.bcs.valiit;

public class Ex1 {

    public static void main(String[] args) {

        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";

        StringBuilder tekst = new StringBuilder();
        tekst.append("\"");
        tekst.append(president1);
        tekst.append(", ");
        tekst.append(president2);
        tekst.append(", ");
        tekst.append(president3);
        tekst.append(", ");
        tekst.append(president4);
        tekst.append(" ja ");
        tekst.append(president5);
        tekst.append(" on Eesti presidendid.\"");

        System.out.println(tekst);



    }


}
