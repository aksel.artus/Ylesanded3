package ee.bsc.valiit;

import java.util.Scanner;

public class Ex2 {

    public static void main(String[] args) {


        //1.Õige

        String str = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
        Scanner scanner = new Scanner(str);
        while (scanner.useDelimiter("Rida: ").hasNext()) {

            System.out.println(scanner.next());
        }

        System.out.println();

        //2. Vale aga töötab ka.

        String[] resultaat2 = str.split("Rida: ");
        for (String element: resultaat2) {
            System.out.println(element);

        }


        //3. näide sõnade arvu leidmiseks tekstist
        String[] sonad = str.split(" ");
        System.out.println("Tekstis oli " + sonad.length + " sõna");
















    }



}
